#include <fstream>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>

// A simple config file reader
using std::string;

class CfgRead {
public:
  CfgRead()
  : m_CfgPath("./config.cfg"),
    m_Delimiter("=")
  {};

  // Set config file location
  void SetPath(string _path) {
    m_CfgPath = _path;
  }

  // Get value for a given key
  string GetValue(string _key) {
    try {
      std::ifstream ifs;
      ifs.open(m_CfgPath);
      string line, key, value;
      size_t pos = 0;

      if(ifs.is_open()) {
        while(getline(ifs, line)) {
          if(line[0] == '#') continue; // Ignore comment
          if( (pos = line.find(m_Delimiter)) != string::npos ) {
            // Split on delimiter and get value
            key = line.substr(0, pos);
            if(key == _key) {
              value = line.substr(pos + 1, line.length() - pos);
              return value;
            }
          } //if
        } //while
      } //if file open
      throw std::runtime_error("Value not found for key: " + _key);
    } catch(std::exception &e) {
      throw std::runtime_error(e.what());
    }
  };

private:
  string m_CfgPath;
  string m_Delimiter;
}; // class CfgRead

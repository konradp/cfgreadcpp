#include <iostream>
#include <stdexcept>
#include "cfgreadcpp.hpp"

int main()
{
  std::cout << "Reading config value for 'key2'" << std::endl;
  CfgRead c;
  c.SetPath("./config.cfg");
  try {
    std::cout << c.GetValue("key2") << std::endl;
  } catch(const std::exception& e) {
    std::cerr << "Failed reading config: " << std::endl
    << e.what() << std::endl;
  }
}
